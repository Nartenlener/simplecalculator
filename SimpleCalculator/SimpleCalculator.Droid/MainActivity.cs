﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using Android.Util;
using Android.Text;
using SimpleCalculator.Droid.Resources;

namespace SimpleCalculator.Droid
{
	[Activity (Label = "Simple Calculator", 
		MainLauncher = true, Icon = "@drawable/ic_launcher", 
		ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{
        CalcButton sqrt2;

        CalcButton number1;
        CalcButton number2;
        CalcButton number3;
        CalcButton backButton;
        CalcButton cancelButton;

        CalcButton number4;
        CalcButton number5;
        CalcButton number6;
        CalcButton multiply;
        CalcButton divide;

        CalcButton number7;
        CalcButton number8;
        CalcButton number9;
        CalcButton plus;
        CalcButton minus;

        CalcButton plusMinus;
        CalcButton number0;
        CalcButton dot;
        CalcButton equal;


        Button leftBracket;
        Button rightBracket;
        Button pow2;
        Button pow3;
        Button powXY;

        TextView txtViewResult;
        TextView txtViewEquation;
        String equation;

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

            #region Init button

            // row ?
            sqrt2 = new CalcButton(this, Resource.Id.buttonSqrt2);

            // row 1
            number1 = new CalcButton(this, Resource.Id.buttonNumber1);
            number2 = new CalcButton(this, Resource.Id.buttonNumber2);
            number3 = new CalcButton(this, Resource.Id.buttonNumber3);
            backButton = new CalcButton(this, Resource.Id.buttonBack);
            cancelButton = new CalcButton(this, Resource.Id.buttonCancel);

            // row 4
            number4 = new CalcButton(this, Resource.Id.buttonNumber4);
            number5 = new CalcButton(this, Resource.Id.buttonNumber5);
            number6 = new CalcButton(this, Resource.Id.buttonNumber6);
            multiply = new CalcButton(this, Resource.Id.buttonMultiply);
            divide = new CalcButton(this, Resource.Id.buttonDivide);

            // row 7
            number7 = new CalcButton(this, Resource.Id.buttonNumber7);
            number8 = new CalcButton(this, Resource.Id.buttonNumber8);
            number9 = new CalcButton(this, Resource.Id.buttonNumber9);
            plus = new CalcButton(this, Resource.Id.buttonPlus);
            minus = new CalcButton(this, Resource.Id.buttonMinus);

            // row +/-
            plusMinus = new CalcButton(this, Resource.Id.buttonNumberPlusMinus);
            number0 = new CalcButton(this, Resource.Id.buttonNumber0);
            dot = new CalcButton(this, Resource.Id.buttonDot);
            equal = new CalcButton(this, Resource.Id.buttonEqual);
            #endregion

            #region set button properties

            // row ?
            sqrt2.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.sqrt2));

            // row 1
            number1.SetText(Resource.Id.textViewCurrentFunction, "1");
            number2.SetText(Resource.Id.textViewCurrentFunction, "2");
            number3.SetText(Resource.Id.textViewCurrentFunction, "3");
            backButton.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.back));
            backButton.SetTextSize(Resource.Id.textViewCurrentFunction, GetString(Resource.Dimension.button_back_text_size));
            cancelButton.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.cancel));
            cancelButton.TextColor(Resource.Id.textViewCurrentFunction, GetString(Resource.Color.cancelbuttontext));

            // row 4
            number4.SetText(Resource.Id.textViewCurrentFunction, "4");
            number5.SetText(Resource.Id.textViewCurrentFunction, "5");
            number6.SetText(Resource.Id.textViewCurrentFunction, "6");
            multiply.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.multiply));
            divide.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.divide));

            // row 7
            number7.SetText(Resource.Id.textViewCurrentFunction, "7");
            number8.SetText(Resource.Id.textViewCurrentFunction, "8");
            number9.SetText(Resource.Id.textViewCurrentFunction, "9");
            plus.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.plus));
            minus.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.minus));

            // row +/-
            plusMinus.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.plus_minus));
            number0.SetText(Resource.Id.textViewCurrentFunction, "0");
            dot.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.comma));
            equal.SetText(Resource.Id.textViewCurrentFunction, GetString(Resource.String.equal));

            #endregion


            // mathematical operations
            leftBracket = FindViewById<Button>(Resource.Id.buttonLeftBracket);
            rightBracket = FindViewById<Button>(Resource.Id.buttonRightBracket);
            pow2 = FindViewById<Button>(Resource.Id.buttonPow2);
            pow3 = FindViewById<Button>(Resource.Id.buttonPow3);
            powXY = FindViewById<Button>(Resource.Id.buttonPowXY);
            powXY.TextFormatted = Html.FromHtml("x<sup>y</sup>");

            // TextViews
            txtViewResult = FindViewById<TextView>(Resource.Id.textViewResult);
            txtViewResult.Text = default(string);
            txtViewEquation = FindViewById<TextView>(Resource.Id.textViewEquation);
            txtViewEquation.Text = default(string);

            // numbers
            number0.Click += NumberButtonClick;
            number1.Click += NumberButtonClick;
            number2.Click += NumberButtonClick;
            number3.Click += NumberButtonClick;
            number4.Click += NumberButtonClick;
            number5.Click += NumberButtonClick;
            number6.Click += NumberButtonClick;
            number7.Click += NumberButtonClick;
            number8.Click += NumberButtonClick;
            number9.Click += NumberButtonClick;

            // mathematical operation
            plus.Click += PlusClick;
            minus.Click += MinusClick;
            multiply.Click += MultiplyClick;           
            divide.Click += DivideClick;
            sqrt2.Click += Sqrt2Click;

            leftBracket.Click += ButtonLeftBracketClick;
            rightBracket.Click += ButtonRightBracketClick;
            pow2.Click += ButtonPow2Click;
            pow3.Click += ButtonPow3Click;
            powXY.Click += ButtonPowXYClick;

            // other eg. cancel
            backButton.Click += BackClick;
            cancelButton.Click += CancelClick;
            equal.Click += EqualClick;
            dot.Click += DotClick;
            plusMinus.Click += PlusMinusClick;

        }

        private void Sqrt2Click(object sender, EventArgs e)
        {
            
        }

        private void ButtonPowXYClick(object sender, EventArgs e)
        {
            string value = "^";

            txtViewEquation.Text += value;
            equation += value;
        }

        private void ButtonPow3Click(object sender, EventArgs e)
        {
            string value = "^3";

            txtViewEquation.Text += value;
            equation += value;
        }

        private void ButtonPow2Click(object sender, EventArgs e)
        {
            string value = "^2";

            txtViewEquation.Text += value;
            equation += value;
        }

        private void ButtonLeftBracketClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.left_bracket);

            txtViewEquation.Text += value;
            equation += value;
        }

        private void ButtonRightBracketClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.right_bracket);

            txtViewEquation.Text += value;
            equation += value;
        }

        private void EqualClick(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(equation))
            {
                try
                {
                    double result = new MathExpression(equation).Calculate();
                    txtViewResult.Text = result.ToString();
                }
                catch(Exception exc)
                {
                    Toast.MakeText(this, "Invalid equation", ToastLength.Short);
                }
            }
            else
            {
                Toast.MakeText(this, "Równanie jest puste", ToastLength.Short).Show();
            }
        }

        private void BackClick(object sender, EventArgs e)
        {
            string value;

            if (!String.IsNullOrEmpty(txtViewEquation.Text))
            {
                value = (txtViewEquation.Text.Length > 1) ? txtViewEquation.Text.Remove(txtViewEquation.Text.Length - 1, 1) : default(string);

                txtViewEquation.Text = value;
                equation = value;
            }
        }

        private void CancelClick(object sender, EventArgs e)
        { 
            txtViewEquation.Text = default(string);
            txtViewResult.Text = default(string);
            equation = default(string);
        }

        private void DivideClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.divide);

            txtViewEquation.Text += value;
            equation += value;
        }

        private void MinusClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.minus);

            txtViewEquation.Text += GetString(Resource.String.minus);
            equation += value;
        }

        private void MultiplyClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.multiply);

            txtViewEquation.Text += value;
            equation += value;
        }

        private void PlusClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.plus);

            txtViewEquation.Text += value;
            equation += value; 
        }

        private void DotClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.comma);

            txtViewEquation.Text += value;
            equation += value;
        }

        private void PlusMinusClick(object sender, EventArgs e)
        {
            string value = GetString(Resource.String.negative);

            if (txtViewEquation.Text.EndsWith(value.ToString()))
            {
                txtViewEquation.Text = (txtViewEquation.Text.Length > 1) ? txtViewEquation.Text.Remove(txtViewEquation.Text.Length - 1) : default(string);
                equation = (equation.Length > 1) ? equation.Remove(txtViewEquation.Text.Length - 1) : default(string);
            }
            else
            {
                txtViewEquation.Text += value;
                equation += value;
            }
        }

        private void NumberButtonClick (object sender, EventArgs args)
        {
            CalcButton cbt = (CalcButton)sender;
            String number = cbt.GetText(Resource.Id.textViewCurrentFunction);

            txtViewEquation.Text += number;
            equation += number;
        }
    }
}


