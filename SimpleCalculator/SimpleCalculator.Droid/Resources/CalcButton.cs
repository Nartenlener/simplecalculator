using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleCalculator.Converters;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace SimpleCalculator.Droid.Resources
{
    // base on http://izvornikod.com/Blog/tabid/82/EntryId/8/Creating-Android-button-with-image-and-text-using-relative-layout.aspx

    class CalcButton : RelativeLayout
    {
        private Context context;

        public CalcButton(Context context, int id)
            : base(context)
        {
            this.context = context;

            // find layout
            View v = ((Activity)context).FindViewById<RelativeLayout>(id);

            if(v.GetType() != typeof(RelativeLayout))
            {
                return;
            }

            RelativeLayout layout = (RelativeLayout)v;

            //copy layout parameters
            ViewGroup.LayoutParams parameters = layout.LayoutParameters;
            this.LayoutParameters = parameters;

            // here I am using temporary instance of Button class
            // to get standard button background and to get button text color
            Button bt = new Button(context);
            this.Background = bt.Background;

            // copy all child from relative layout to this button
            while(layout.ChildCount > 0)
            {
                View child = layout.GetChildAt(0);
                layout.RemoveViewAt(0);
                this.AddView(child);

                // if child is textView set its color to standard buttong text colors
                // using temporary instance of Button class
                if(child.GetType() == typeof(TextView))
                {
                    ((TextView)child).SetTextColor(bt.TextColors);
                }

                // just to be sure that child views can't be clicked and focused
                child.Clickable = false;
                child.Focusable = false;
                child.FocusableInTouchMode = false;
            }

            // remove all view from layout (maybe it's not necessary)
            layout.RemoveAllViews();

            // set that this button is clickable, focusable, ...
            this.Clickable = true;
            this.Focusable = true;

            // replace relative layout in parent with this one modified to looks like button
            ViewGroup vp = (ViewGroup)layout.Parent;
            int index = vp.IndexOfChild(layout);
            vp.RemoveView(layout);
            vp.AddView(this, index);

            this.Id = id;
        }

        //method for setting text for the textViews
        public void SetText(int id, string text)
        {
            View v = FindViewById<TextView>(id);

            if(v != null && v.GetType() == typeof(TextView))
            {
                ((TextView)v).Text = text;
            }
        }

        //method for setting text for the textViews
        public string GetText(int id)
        {
            View v = FindViewById<TextView>(id);
            string returningTxt = default(string);

            if (v != null && v.GetType() == typeof(TextView))
            {
                returningTxt = ((TextView)v).Text;
            }

            return returningTxt;
        }

        //method for setting text for the textViews
        public void SetTextSize(int id, string txtSize)
        {
            View v = FindViewById<TextView>(id);

            if (v != null && v.GetType() == typeof(TextView))
            {
                ((TextView)v).TextSize = txtSize.PixelsToDp(context);
            }
        }

        //method for setting textColor for the textViews
        public void TextColor(int id, string color)
        {
            View v = FindViewById<TextView>(id);

            if (v != null && v.GetType() == typeof(TextView))
            {
                ((TextView)v).SetTextColor(Color.ParseColor(color));
            }
        }
    }
}