﻿using Android.Content;
using Android.Util;
using System;

namespace SimpleCalculator.Converters
{
    public static class Convert
    {
        public static float PixelsToDp(this string pix, Context context)
        {
            DisplayMetrics displayMetrics = context.Resources.DisplayMetrics;
            float pixWidth = context.Resources.DisplayMetrics.WidthPixels;

            return (float)Math.Round((displayMetrics.Xdpi / (float)DisplayMetricsDensity.Default) * int.Parse(pix.Split('.')[0]));
        }
    }
}
