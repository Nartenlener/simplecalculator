﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    public static class MathHelper
    {
        public static char negativeSign = '\u002d';
        public static char minusSign = '\u2012';
        public static char decimalSeparator = '\u002C';

        public static NumberFormatInfo numberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = decimalSeparator.ToString(), NegativeSign = negativeSign.ToString(), NumberNegativePattern = 1 };

        public static double Calculate(Queue<MathValOrOp> posfix)
        {
            Stack<MathValOrOp> value = new Stack<MathValOrOp>();
            

            foreach(var p in posfix)
            {
                string valOrOp = p.ToString();

                if (valOrOp.IsDouble())
                {
                    value.Push(new MathValOrOp(valOrOp));
                }
                else if(valOrOp.Equals("+"))
                {
                    double number1 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    double number2 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    string result = (number2 + number1).ToString(numberFormatInfo);

                    value.Push(new MathValOrOp(result));
                }
                else if (valOrOp.Equals(minusSign.ToString()))
                {
                    double number1 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    double number2 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    string result = (number2 - number1).ToString(numberFormatInfo);

                    value.Push(new MathValOrOp(result));
                }
                else if (valOrOp.Equals("/"))
                {
                    double number1 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    double number2 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    string result = (number2 / number1).ToString(numberFormatInfo);

                    value.Push(new MathValOrOp(result));
                }
                else if (valOrOp.Equals("*"))
                {
                    double number1 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    double number2 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    string result = (number2 * number1).ToString(numberFormatInfo);

                    value.Push(new MathValOrOp(result));
                }
                else if (valOrOp.Equals("^"))
                {
                    double number1 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    double number2 = double.Parse(value.Pop().ToString(), numberFormatInfo);
                    string result = Math.Pow(number2, number1).ToString(numberFormatInfo);

                    value.Push(new MathValOrOp(result));
                }
            }

            return double.Parse(value.Pop().ToString(), numberFormatInfo);
        }

        public static Queue<MathValOrOp> InfixToPostfix(string expression)
        {
            // Ending of expression. Sharp is required because if the last character is a digit, 
            // the loop is broken and does not transfer it this number to the queue. Transfer is doing in next loop
            expression += "#";

            Queue<MathValOrOp> input = ParseExpresion(expression.ToCharArray());
            Queue<MathValOrOp> output = new Queue<MathValOrOp>();
            Stack<string> stack = new Stack<string>();

            int priority = 0;

            foreach(var q in input)
            {
                priority = DesignedOperationPriority(q.ToString());
                string valOrOp = q.ToString();

                if (valOrOp.IsDouble())
                {
                    output.Enqueue(new MathValOrOp(valOrOp));
                }
                else
                {
                    // If char == #, loop is on end expression, so it breaks
                    if (valOrOp.Equals("#"))
                    {
                        break;
                    }

                    if (valOrOp.Equals("("))
                    {
                        stack.Push(valOrOp);
                    }
                    else if (valOrOp.Equals(")"))
                    {
                        do
                        {
                            output.Enqueue(new MathValOrOp(stack.Pop()));
                        } while (!stack.Peek().Equals("("));

                        // remove bracket from stack
                        stack.Pop();
                    }
                    else if (priority > ((stack.Count == 0) ? 0 : DesignedOperationPriority(stack.Peek())))
                    {
                        stack.Push(valOrOp);
                    }
                    else if (priority <= ((stack.Count == 0) ? 0 : DesignedOperationPriority(stack.Peek())))
                    {
                        output.Enqueue(new MathValOrOp(stack.Pop()));
                        stack.Push(valOrOp);
                    }
                }
            }

            do
            {
                if (stack.Count > 0)
                {
                    output.Enqueue(new MathValOrOp(stack.Pop()));
                }
            } while (stack.Count() > 0);

            return output;
        }

        private static Queue<MathValOrOp> ParseExpresion(Array arrayString)
        {
            int i;
            bool isNegative = false;
            Queue<MathValOrOp> result = new Queue<MathValOrOp>();

            // Store a parts of number
            string number = default(string);

            for (i = 0; i < arrayString.Length; i++)
            {
                string valOrOp = arrayString.GetValue(i).ToString();

                if (valOrOp.Equals("(") || valOrOp.Equals(")") || valOrOp.Equals("#"))
                {
                    // Add number
                    if (!String.IsNullOrEmpty(number))
                    {
                        result.Enqueue(new MathValOrOp(number));
                        number = default(string);
                    }

                    // negative bracket
                    if(isNegative)
                    {
                        // -1
                        result.Enqueue(new MathValOrOp(negativeSign.ToString() + "1"));
                        // *
                        result.Enqueue(new MathValOrOp("*"));
                        isNegative = false;
                    }

                    result.Enqueue(new MathValOrOp(valOrOp));
                }
                else if(valOrOp.IsDouble() || valOrOp.Equals(decimalSeparator.ToString()))
                {
                    number += (isNegative) ? negativeSign + valOrOp : valOrOp;
                    isNegative = false;
                }
                else if(IsMathOperation(valOrOp))
                {
                    // Add number
                    if (!String.IsNullOrEmpty(number))
                    {
                        result.Enqueue(new MathValOrOp(number));
                        number = default(string);
                    }

                    // Add operation
                    result.Enqueue(new MathValOrOp(valOrOp));
                }
                else if(valOrOp.Equals(negativeSign.ToString()))
                {
                    isNegative = true;
                }
            }

            return result;
        }

        private static int DesignedOperationPriority(String q)
        {
            int priority;
            if (q.Equals("(") || q.IsDouble() || q.Equals("#"))
            {
                priority = 0;
            }
            else if (q.Equals("+") || q.Equals(minusSign.ToString()) || q.Equals(")"))
            {
                priority = 1;
            }
            else if (q.Equals("*") || q.Equals("/") || q.Equals("%"))
            {
                priority = 2;
            }
            else if (q.Equals("^"))
            {
                priority = 3;
            }
            else
            {
                throw new Exception($"Unsupported type of operation: {q.ToString()}");
            }

            return priority;
        }

        private static bool IsMathOperation(string valOrOp)
        {
            return (valOrOp.Equals("+") || valOrOp.Equals(minusSign.ToString()) || valOrOp.Equals("*") || valOrOp.Equals("/") || valOrOp.Equals("^")) ? true : false;
        }

        public static bool IsDouble(this string input)
        {
            double refDouble;

            return double.TryParse(input, NumberStyles.Number, numberFormatInfo, out refDouble);
                
         }
    }
}
