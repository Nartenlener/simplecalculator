﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCalculator.Interfaces;

namespace SimpleCalculator
{
    public class MathExpression : ICalculate 
    {
        private string expression;

        public MathExpression(string expression)
        {
            this.expression = expression;
        }

        public double Calculate()
        {
            return MathHelper.Calculate(MathHelper.InfixToPostfix(expression));
        }
    }
}
