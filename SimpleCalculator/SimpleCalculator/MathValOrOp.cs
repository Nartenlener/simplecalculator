﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    public class MathValOrOp
    {
        private string valOrOp;

        public MathValOrOp(string valOrOp)
        {
            this.valOrOp = valOrOp;
        }

        public override string ToString()
        {
            return valOrOp;
        }
    }
}
