﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleCalculator;
using System.Collections.Generic;

namespace SimpleCalculatorUnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InfixToPostfixTest_1()
        {
            Queue<string> pattern = new Queue<string>();
            pattern.Enqueue("5");
            pattern.Enqueue("2");
            pattern.Enqueue("4");
            pattern.Enqueue("+");
            pattern.Enqueue("2");
            pattern.Enqueue("^");
            pattern.Enqueue("*");

            string expression = "5*(2+4)^2";

            for (int i = 0; i < pattern.Count; i++)
            {
                Assert.AreEqual(pattern.ToArray()[i].ToString(), MathHelper.InfixToPostfix(expression).ToArray()[i].ToString());
            }
        }

        [TestMethod]
        public void InfixToPostfixTest_2()
        {
            Queue<string> pattern = new Queue<string>();
            pattern.Enqueue("5");
            pattern.Enqueue("2");
            pattern.Enqueue("4");
            pattern.Enqueue("+");
            pattern.Enqueue("*");
            pattern.Enqueue("3");
            pattern.Enqueue("/");
            pattern.Enqueue("28");
            pattern.Enqueue("2");
            pattern.Enqueue("3");
            pattern.Enqueue("/");
            pattern.Enqueue("*");
            pattern.Enqueue("+");

            string expression = "(5*(2+4))/3+28*(2/3)";

            for (int i = 0; i < pattern.Count; i++)
            {
                Assert.AreEqual(pattern.ToArray()[i].ToString(), MathHelper.InfixToPostfix(expression).ToArray()[i].ToString());
            }
        }

        [TestMethod]
        public void CalculateTest_1()
        {
            //"5*(2+4)^2";
            Queue<MathValOrOp> pattern = new Queue<MathValOrOp>();
            pattern.Enqueue(new MathValOrOp("5"));
            pattern.Enqueue(new MathValOrOp("2"));
            pattern.Enqueue(new MathValOrOp("4"));
            pattern.Enqueue(new MathValOrOp("+"));
            pattern.Enqueue(new MathValOrOp("2"));
            pattern.Enqueue(new MathValOrOp("^"));
            pattern.Enqueue(new MathValOrOp("*"));

            Assert.AreEqual(MathHelper.Calculate(pattern), 180);
        }

        [TestMethod]
        public void CalculateTest_2()
        {
            //"(5*(2+4))/3+28*(2/3)";
            Queue<MathValOrOp> pattern = new Queue<MathValOrOp>();
            pattern.Enqueue(new MathValOrOp("5"));
            pattern.Enqueue(new MathValOrOp("2"));
            pattern.Enqueue(new MathValOrOp("4"));
            pattern.Enqueue(new MathValOrOp("+"));
            pattern.Enqueue(new MathValOrOp("*"));
            pattern.Enqueue(new MathValOrOp("3"));
            pattern.Enqueue(new MathValOrOp("/"));
            pattern.Enqueue(new MathValOrOp("28"));
            pattern.Enqueue(new MathValOrOp("2"));
            pattern.Enqueue(new MathValOrOp("3"));
            pattern.Enqueue(new MathValOrOp("/"));
            pattern.Enqueue(new MathValOrOp("*"));
            pattern.Enqueue(new MathValOrOp("+"));

            Assert.AreEqual(Math.Round(MathHelper.Calculate(pattern),4), 28.6667);
        }

        [TestMethod]
        public void CalculateTest_3()
        {
            //"5*(0.2+4.234)^2";
            Queue<MathValOrOp> pattern = new Queue<MathValOrOp>();
            pattern.Enqueue(new MathValOrOp("5"));
            pattern.Enqueue(new MathValOrOp("0.2"));
            pattern.Enqueue(new MathValOrOp("4.234"));
            pattern.Enqueue(new MathValOrOp("+"));
            pattern.Enqueue(new MathValOrOp("2"));
            pattern.Enqueue(new MathValOrOp("^"));
            pattern.Enqueue(new MathValOrOp("*"));

            Assert.AreEqual(MathHelper.Calculate(pattern), 98.30178);
        }

        [TestMethod]
        public void FinalCalculateTest_1()
        {
            string expression = "(5*(2+4))/3+28*(2/3)";

            Assert.AreEqual(Math.Round(new MathExpression(expression).Calculate(),4), 28.6667);
        }
    }
}
